using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridManager : MonoBehaviour {
  public int size = 20;
  private float tileSize = 1;
  public Transform player;
  public int mushroomCount = 20;
  [SerializeField] private GameObject mushroom;
  public Transform centipedeSpawner;
  void Awake() {
    float gridSize = size * tileSize;
    GenerateGrid();
    GenerateMushroom();
    CenterMapPosition(gridSize);
    SetPlayerSpawn(gridSize);
    SetCentipedeSpawn(gridSize);
  }
  private void GenerateGrid() {
    int playerBorder = size - (int)Mathf.Floor(size * 0.15f);

    for (int row = 0; row < size + 2; row++) {
      for (int col = 0; col < size + 2; col++) {
        Vector2 position = new Vector2(col * tileSize, row * -tileSize);

        if (row == 0 || col == 0 || row == size + 1 || col == size + 1) {
          //wall at border
          AddGridItem("GridWall", position);
        } else {
          // floor
          AddGridItem("GridFloor", position);
        }

        if (row == playerBorder) {
          // player hidden wall
          AddGridItem("HiddenWall", position);
        }
      }
    }
  }

  private void SetCentipedeSpawn(float gridSize) {
    float centipedeX = (gridSize % 2) * (-tileSize / 2) + (tileSize / 2);
    float centipedeY = gridSize / 2 - tileSize / 2;
    centipedeSpawner.transform.position = new Vector2(centipedeX, centipedeY);
    centipedeSpawner.GetComponent<CentipedeSpawn>().GenerateCentipede();
  }

  private void GenerateMushroom() {
    for (int mushroomNumber = 0; mushroomNumber < mushroomCount; mushroomNumber++) {
      Vector2 position = new Vector2(
        Mathf.Floor(Random.Range(1f, size - 1)),
        -Mathf.Floor(Random.Range(1f, size - 1))
        );
      GameObject item = Instantiate(mushroom, transform);
      item.transform.position = position;
    }
  }

  private void AddGridItem(string resourceName, Vector2 position) {
    // add item to grid
    GameObject item = (GameObject)Instantiate(Resources.Load(resourceName), transform);
    item.transform.position = position;
  }

  private void CenterMapPosition(float gridSize) {
    // center map position
    transform.position = new Vector2(
      -gridSize / 2 - 1 + tileSize / 2,
      gridSize / 2 + 1 - tileSize / 2
    );

    // move camera to view all map
    Camera.main.orthographicSize = (float)size / 2 + 0.5f;
  }

  private void SetPlayerSpawn(float gridSize) {
    // move player to bottom of map
    float playerX = (gridSize % 2) * (-tileSize / 2) + (tileSize / 2);
    float playerY = -gridSize / 2 + tileSize / 2;
    player.transform.position = new Vector2(playerX, playerY);
  }
}
