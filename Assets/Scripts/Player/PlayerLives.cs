using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLives : MonoBehaviour {
  private int lives;
  private void Start() {
    lives = GetComponent<HealthSystem>().totalLife;
  }

  private void Update() {
    if (lives <= 0) {
      Debug.Log("Player Dead!");
      PlayerDeadHandler();
    }
  }

  private void PlayerDeadHandler() {
    Time.timeScale = 0f;
    Destroy(gameObject);
  }
}
