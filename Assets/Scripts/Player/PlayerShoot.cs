using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShoot : MonoBehaviour {
  [SerializeField] private Transform bulletPrefab;
  private GridMovement gridMovement;
  void Start() {
    gridMovement = GetComponent<GridMovement>();
  }
  void Update() {
    if (InputManager.Instance.GetShootInput()) {
      StartCoroutine(ShootBullet());
    }
  }

  private IEnumerator ShootBullet() {
    Transform bulletTransform = Instantiate(bulletPrefab, gridMovement.movePoint.position, Quaternion.identity);
    yield return new WaitForSeconds(0.5f);
  }
}
