using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerTrigger : MonoBehaviour {
  [SerializeField] private GameObject playerParent;

  private void OnTriggerEnter2D(Collider2D collider) {
    if (collider.GetComponent<HealthSystem>() != null) {
      HealthSystem parentSystem = playerParent.GetComponent<HealthSystem>();
      parentSystem.TakeDamage(1);
      Debug.Log("Take 1 Damage");
    }
  }
}
