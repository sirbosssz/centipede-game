using UnityEngine;
using System.Collections.Generic;

public class PlayerMovement : MonoBehaviour {
  public float moveSpeed = 5f;
  public LayerMask obstacles;
  private GridMovement gridMovement;
  void Start() {
    gridMovement = GetComponent<GridMovement>();
  }

  void Update() {
    MoveHandler();
  }

  private void MoveHandler() {
    // grid check
    if (gridMovement.IsMoveInGrid()) {
      if (!Physics2D.OverlapCircle(
        gridMovement.movePoint.position + InputManager.Instance.GetMoveInput(),
        .2f,
        obstacles
      )) {
        gridMovement.movePoint.position += InputManager.Instance.GetMoveInput();
      }
    }
  }
}
