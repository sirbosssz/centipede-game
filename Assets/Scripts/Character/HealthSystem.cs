using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthSystem : MonoBehaviour {
  public int totalLife = 3;
  public bool canTakeDamage = true;
  private enum objectType { Player, Mushroom, CentipedeHead, CentipedeBody };
  [SerializeField] private objectType type;

  void Update() {
    if (totalLife <= 0) {
      Destroy(gameObject);
    }
  }

  public void TakeDamage(int damage) {
    if (canTakeDamage) {
      totalLife -= damage;
    }
  }
}
