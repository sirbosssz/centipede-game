using UnityEngine;

public class GridMovement : MonoBehaviour {
  public Transform movePoint;
  private float moveSpeed;
  // Start is called before the first frame update
  void Start() {
    movePoint.parent = null;
  }

  // Update is called once per frame
  void Update() {
    if (GetComponent<PlayerMovement>() != null) {
      moveSpeed = GetComponent<PlayerMovement>().moveSpeed;
    } else if (GetComponent<BulletManager>() != null) {
      moveSpeed = GetComponent<BulletManager>().moveSpeed;
    } else {
      moveSpeed = 3f;
    }

    transform.position = Vector3.MoveTowards(transform.position, movePoint.position, moveSpeed * Time.deltaTime);
  }

  public bool IsMoveInGrid() {
    // return true when character move nearby grid (distance <= 0.05)
    if (Vector3.Distance(transform.position, movePoint.position) <= 0.05f) {
      return true;
    }
    return false;
  }
}
