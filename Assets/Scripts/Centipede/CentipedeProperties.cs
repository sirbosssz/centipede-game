using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CentipedeProperties : MonoBehaviour {

  public GameObject centipedeSprite;
  private bool isHead;

  private GameObject partBefore;
  private GameObject partAfter;

  public void Setup(int partNumber) {
    // set name
    name = "Centipede Part " + partNumber;

    // set sprite sorting
    SpriteRenderer sprite = centipedeSprite.GetComponent<SpriteRenderer>();
    sprite.sortingOrder = 4;

    // check isHead
    if (partNumber == 0) {
      isHead = true;
      sprite.sprite = CentipedeManager.Instance.centipedeHead;
    } else {
      isHead = false;
      sprite.sprite = CentipedeManager.Instance.centipedeBody;
    }
  }

  public void SetPartBefore(GameObject part) {
    partBefore = part;
  }

  public void SetPartAfter(GameObject part) {
    partAfter = part;
  }

  public GameObject GetPartBefore() {
    return partBefore;
  }
  public GameObject GetPartAfter() {
    return partAfter;
  }

  public bool IsHead() {
    return isHead;
  }
}
