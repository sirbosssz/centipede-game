using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CentipedeSpawn : MonoBehaviour {
  public int size = 15;
  [SerializeField] private GameObject centipede;
  public List<GameObject> partsList;

  public void GenerateCentipede() {
    for (int partNumber = 0; partNumber < size; partNumber++) {
      GameObject part = GeneratePart(partNumber);
      partsList.Add(part);

      if (partNumber > 0) {
        partsList[partNumber].GetComponent<CentipedeProperties>().SetPartBefore(partsList[partNumber - 1]);
        partsList[partNumber - 1].GetComponent<CentipedeProperties>().SetPartAfter(partsList[partNumber]);
      }
    }
  }

  private GameObject GeneratePart(int partNumber) {
    GameObject part = (GameObject)Instantiate(centipede, transform.position, Quaternion.identity);
    part.GetComponent<CentipedeProperties>().Setup(partNumber);
    if (partNumber != 0) {
      // part.transform.position += Vector3.up;
    }
    return part;
  }
}
