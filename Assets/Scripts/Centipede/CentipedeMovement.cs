using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CentipedeMovement : MonoBehaviour {
  public float moveSpeed = 5f;
  public LayerMask obstacles;
  private GridMovement gridMovement;
  private CentipedeProperties centipedeProperties;

  private Vector3 stuckDirection = Vector3.zero;
  private Vector3 lastVerticalDirection = Vector3.zero;
  private bool isMovingDown = true;

  public Vector3 moveDirection = Vector3.zero;
  public Vector3 lastMove;
  void Start() {
    gridMovement = GetComponent<GridMovement>();
    centipedeProperties = GetComponent<CentipedeProperties>();

    if (centipedeProperties.IsHead()) {
      moveDirection = Vector3.left;
    }
    lastMove = gridMovement.movePoint.position;
  }

  void Update() {
    if (centipedeProperties.IsHead()) {
      MoveHandler();
    } else {
      FollowHead();
    }
  }

  private void MoveHandler() {
    if (gridMovement.IsMoveInGrid()) {
      if (CanMoveForward(moveDirection)) {
        StartCoroutine(Move(moveDirection));
        if (moveDirection == Vector3.down) {
          moveDirection = Vector3.zero - stuckDirection;
        } else if (moveDirection == Vector3.up) {
          moveDirection = Vector3.zero - stuckDirection;
        }
        // Debug.Log("Share Location " + moveDirection);
      } else {
        stuckDirection = moveDirection;
        if (moveDirection == Vector3.left || moveDirection == Vector3.right) {
          lastVerticalDirection = moveDirection;
          if (isMovingDown) {
            moveDirection = Vector3.down;
          } else {
            moveDirection = Vector3.up;
          }
        } else if (moveDirection == Vector3.down) {
          isMovingDown = false;
          moveDirection = Vector3.up;
          stuckDirection = lastVerticalDirection;
        } else if (moveDirection == Vector3.up) {
          isMovingDown = true;
          moveDirection = Vector3.down;
          stuckDirection = lastVerticalDirection;
        }
      }
    } else {
    }
  }

  private bool CanMoveForward(Vector3 direction) {
    if (!Physics2D.OverlapCircle(
      gridMovement.movePoint.position + direction,
      .2f,
      obstacles
    )) { return true; }
    return false;
  }

  private IEnumerator Move(Vector3 direction) {
    gridMovement.movePoint.position += direction;
    if (direction == Vector3.left) {
      gridMovement.movePoint.eulerAngles = new Vector3(0f, 0f, 90f);
    }
    if (direction == Vector3.right) {
      gridMovement.movePoint.eulerAngles = new Vector3(0f, 0f, 270f);
    }
    if (direction == Vector3.up) {
      gridMovement.movePoint.eulerAngles = new Vector3(0f, 0f, 0f);
    }
    if (direction == Vector3.down) {
      gridMovement.movePoint.eulerAngles = new Vector3(0, 0, 180);
    }
    yield return new WaitForEndOfFrame();
    lastMove = gridMovement.movePoint.position;
  }

  private void FollowHead() {
    GameObject partBefore = centipedeProperties.GetPartBefore();
    Vector3 positionBefore = partBefore.GetComponent<GridMovement>().movePoint.position;
    Vector3 moveDirectionBefore = partBefore.GetComponent<CentipedeMovement>().lastMove;
    if (gridMovement.IsMoveInGrid()) {
      if (moveDirectionBefore != positionBefore) {
        gridMovement.movePoint.position = moveDirectionBefore;
      }
      lastMove = moveDirectionBefore;
    }

  }
}
