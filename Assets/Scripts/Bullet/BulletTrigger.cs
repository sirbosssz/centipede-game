using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletTrigger : MonoBehaviour {
  [SerializeField] private GameObject bulletParent;

  private void OnTriggerEnter2D(Collider2D collider) {
    if (collider.GetComponent<HealthSystem>() != null) {
      // Hit
      collider.GetComponent<HealthSystem>().TakeDamage(1);
      bulletParent.GetComponent<BulletManager>().DestroyBullet();
    }
  }
}
