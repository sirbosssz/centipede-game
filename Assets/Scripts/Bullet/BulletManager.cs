using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletManager : MonoBehaviour {
  public float moveSpeed = 5f;
  public LayerMask obstacles;
  private GridMovement gridMovement;

  private void Start() {
    gridMovement = GetComponent<GridMovement>();
  }
  void Update() {
    if (gridMovement.IsMoveInGrid()) {
      if (Physics2D.OverlapCircle(gridMovement.movePoint.position + Vector3.up, .2f, obstacles)) {
        DestroyBullet();
      } else {
        gridMovement.movePoint.position += Vector3.up;
      }
    }
  }

  public void DestroyBullet() {
    Destroy(gridMovement.movePoint.gameObject);
    Destroy(gameObject);
  }
}
