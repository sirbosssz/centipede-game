using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CentipedeManager : MonoBehaviour {
  public static CentipedeManager Instance { get { return _instance; } }
  private static CentipedeManager _instance;

  public Sprite centipedeHead;
  public Sprite centipedeBody;

  private void Awake() {
    if (_instance != null && _instance != this) {
      Destroy(this.gameObject);
      return;
    }
    if (_instance == null) {
      _instance = this;
      DontDestroyOnLoad(this.gameObject);
    }
  }

  private void OnDestroy() {
    Destroy();
  }

  public void Destroy() {
    GameObject.Destroy(_instance.gameObject);
    _instance = null;
  }
}
