using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {
  public Text scoreText;
  public int scoreNumber = 0;
  void Update() {
    scoreText.text = scoreNumber.ToString();

    // todo observer to update score
  }
}
