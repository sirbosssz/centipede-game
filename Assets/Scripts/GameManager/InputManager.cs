using UnityEngine;

public class InputManager : MonoBehaviour {
  public static InputManager Instance { get { return _instance; } }
  private static InputManager _instance;
  private void Awake() {
    if (_instance != null && _instance != this) {
      Destroy(this.gameObject);
      return;
    }
    if (_instance == null) {
      _instance = this;
      DontDestroyOnLoad(this.gameObject);
    }
  }
  private void OnDestroy() {
    Destroy();
  }
  public void Destroy() {
    GameObject.Destroy(_instance.gameObject);
    _instance = null;
  }

  public Vector3 GetMoveInput() {
    Vector3 moveDir = Vector3.zero;

    // Horizontal movement
    if (Input.GetKey(KeyCode.RightArrow)) {
      moveDir.x += 1;
    } else if (Input.GetKey(KeyCode.LeftArrow)) {
      moveDir.x -= 1;
    }

    // Vertical movement
    if (Input.GetKey(KeyCode.UpArrow)) {
      moveDir.y += 1;
    } else if (Input.GetKey(KeyCode.DownArrow)) {
      moveDir.y -= 1;
    }

    return moveDir;
  }

  public bool GetShootInput() {
    if (Input.GetKeyDown(KeyCode.Space)) {
      return true;
    }
    return false;
  }
}
